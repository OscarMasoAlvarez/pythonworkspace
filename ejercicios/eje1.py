import requests
import json
# Librerias necesarias para la ejecucción de este programa

for i in range(1, 10):
    # Bucle de 1 a  maximo 731 veces recoremos el codigo de mas abajo
    response = requests.get("https://superheroapi.com/api/1221777054692745/"+str(i))
    # Recogemos la URL a la que vamos a acceder para recoger el Json se compone de url --> "https://superheroapi.com/api/" y mi codigo --> 1221777054692745
    print(response.status_code)
    # Sacamos por pantalla el Status a la llamada Api
    print(response.json())
    # Sacamos por pantalla la respuesta del Json
    ruta = "jsons/" + "Heroe{0}".format(i) + ".json"
    # La variable ruta es utilizada para poner el nombre del fichero que itera con la variable i para que no sea el mismo nombre
    with open(ruta, 'w') as jsonFile:
        # Abrimos File(Archivo) con permisos de w (Write) le ponemos nombre a esto --> jsonFile
        json.dump(response.json(), jsonFile)   
        # Mandamos archivo como un Json al archivo abierto mas arriba
        jsonFile.close()
        # Cerramos archivo